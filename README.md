# waifu-hypernetworks

Hypernetworks trained using [stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui).

## [onono-wd4](https://gitlab.com/a00001c/waifu-hypernetworks/-/blob/main/hypernetworks/onono-wd4-4-9000.pt)
![](hypernetworks/onono-wd4-4-9000.png)  
trained on 11 pictures using Waifu Diffusion v1.4.
```
masterpiece, best quality, 1girl, solo, red eyes, black hair, medium hair, closed mouth, white tank top, black pants, navel, medium breasts, simple background, upper body
Negative prompt: worst quality, low quality, medium quality, deleted, lowres, comic, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, jpeg artifacts, signature, watermark, username, blurry
Steps: 20, Sampler: Euler a, CFG scale: 7, Seed: 182383362, Size: 512x768, Model hash: 8cbce067, Hypernet: onono-wd4-4, Hypernet hash: b9eacafc
```


## [ishi-wd4](https://gitlab.com/a00001c/waifu-hypernetworks/-/blob/main/hypernetworks/ishi-wd4-11000.pt)
![](hypernetworks/ishi-wd4-11000.png)  
trained on 13 pictures using Waifu Diffusion v1.4.
```
masterpiece, best quality, 1girl, solo, red eyes, black hair, medium hair, closed mouth, white tank top, black pants, navel, medium breasts, simple background, upper body
Negative prompt: worst quality, low quality, medium quality, deleted, lowres, comic, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, jpeg artifacts, signature, watermark, username, blurry
Steps: 20, Sampler: Euler a, CFG scale: 7, Seed: 182383362, Size: 512x768, Model hash: 8cbce067, Hypernet: ishi-wd4, Hypernet hash: af6f4ae4
```


## [ichi-wd4](https://gitlab.com/a00001c/waifu-hypernetworks/-/blob/main/hypernetworks/ichi-wd4-2-9000.pt)
![](hypernetworks/ichi-wd4-2-9000.png)  
trained on 12 pictures using Waifu Diffusion v1.4.
```
masterpiece, best quality, 1girl, solo, red eyes, black hair, medium hair, closed mouth, white tank top, black pants, navel, medium breasts, simple background, upper body
Negative prompt: worst quality, low quality, medium quality, deleted, lowres, comic, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, jpeg artifacts, signature, watermark, username, blurry
Steps: 20, Sampler: Euler a, CFG scale: 7, Seed: 182383362, Size: 512x768, Model hash: 8cbce067, Hypernet: ichi-wd4-2, Hypernet hash: ef47a15b
```